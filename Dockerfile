FROM mcr.microsoft.com/dotnet/sdk:3.1 AS builder
WORKDIR /src
EXPOSE 80
COPY . .

#Restore Dependencies
RUN dotnet restore "Banterdome.Api.Authentication/Banterdome.Api.Authentication.csproj" -s "https://www.nuget.org/api/v2/"
RUN dotnet restore "Banterdome.Api.Authentication.Tests/Banterdome.Api.Authentication.Tests.csproj" -s "https://www.nuget.org/api/v2/" 
#Run Tests
RUN dotnet test "Banterdome.Api.Authentication.Tests/Banterdome.Api.Authentication.Tests.csproj" /p:CollectCoverage=true
#Publish App
RUN dotnet publish "Banterdome.Api.Authentication/Banterdome.Api.Authentication.csproj" -c Release -o /app/publish
FROM mcr.microsoft.com/dotnet/sdk:3.1  AS final
#Copy App and Run
WORKDIR /app
ENV ASPNETCORE_URLS=http://+:80
COPY --from=builder /app/publish /app
ENTRYPOINT ["dotnet", "Banterdome.Api.Authentication.dll"]
