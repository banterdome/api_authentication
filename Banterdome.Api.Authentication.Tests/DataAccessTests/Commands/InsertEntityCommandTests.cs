﻿using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Moq;
using Banterdome.Api.Authentication.Data;
using Banterdome.Api.Authentication.DataAccess;
using Banterdome.Api.Authentication.DataAccess.Commands;
using Banterdome.Api.Authentication.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Banterdome.Api.Authentication.Tests.DataAccessTests.Commands
{
    public class InsertEntityCommandTests : TestBase<InsertEntityCommand>
    {
        [Fact]
        public async Task ExecuteInsertsIntoDatabase()
        {
            var login = new Login() { Username = "John", Key = 1 };
            var sut = CreateTestSubject();
            var dataContextOptions = new DbContextOptionsBuilder()
                .UseInMemoryDatabase("DataContext")
                .Options;

            using (var context = new DataContext(dataContextOptions))
            {
                this.AutoMocker.GetMock<IDataContextFactory>()
                    .Setup(me => me.Create())
                    .Returns(context);

                await sut.Execute(login);
            }

            using (var context = new DataContext(dataContextOptions))
            {
                Assert.Equal(1, context.Logins.Count());
                Assert.Equal(login.Username, context.Logins.FirstOrDefault().Username);
            }
        }
    }
}
