﻿namespace Banterdome.Api.Authentication.Tests.DataAccessTests.Query
{
    using MockQueryable.Moq;
    using Moq;
    using Banterdome.Api.Authentication.DataAccess;
    using Banterdome.Api.Authentication.DataAccess.Entities;
    using Banterdome.Api.Authentication.DataAccess.Query;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;

    public class GetLoginByUsernameQueryTests : TestBase<GetLoginByUsernameQuery>
    {
        private readonly Mock<Microsoft.EntityFrameworkCore.DbSet<Login>> logins;

        public GetLoginByUsernameQueryTests()
        {
            logins = new List<Login>()
            {
                new Login { Username = "John" },
                new Login { Username = "Steve" },
                new Login { Username = "Gary" }
            }.AsQueryable().BuildMockDbSet();
        }

        [Theory]
        [InlineData("john")]
        [InlineData("JOHN")]
        [InlineData("John")]
        public async Task GetLoginByUsernameQueryReturnsCorrectResults(string Username)
        {

            var mockContext = new Mock<IDataContext>();
            mockContext.Setup(c => c.Logins).Returns(logins.Object);

            this.AutoMocker.GetMock<IDataContextFactory>()
                .Setup(x => x.Create())
                .Returns(mockContext.Object);

            var sut = CreateTestSubject();
            var result = await sut.Execute(Username);

            Assert.NotNull(result);
            Assert.Equal(Username.ToLower(), result.Username.ToLower());
        }

        [Fact]
        public async Task GetLoginByUsernameReturnsSafelyWhenLoginNotFound()
        {
            var stubUsername = "Gary@aol.com";

            var mockContext = new Mock<IDataContext>();
            mockContext.Setup(c => c.Logins).Returns(logins.Object);

            this.AutoMocker.GetMock<IDataContextFactory>()
                .Setup(x => x.Create())
                .Returns(mockContext.Object);

            var sut = CreateTestSubject();
            var result = await sut.Execute(stubUsername);

            Assert.Null(result);
        }
    }
}
