﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using Banterdome.Api.Authentication.Business;
using Banterdome.Api.Authentication.Controllers;
using Banterdome.Api.Authentication.DataAccess.Query;
using Banterdome.Api.Authentication.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Microsoft.AspNetCore.Http;

namespace Banterdome.Api.Authentication.Tests.Controllers
{
    public class AuthenticationControllerTests : TestBase<AuthenticationController>
    {
        [Theory]
        [InlineData(null)]
        [InlineData(0)]
        [InlineData(1)]
        public async Task CreateUserReturnsBadRequestIfSignupIsNullOrContainsNullField(int? index)
        {
            var signups = new List<SignUp>()
            {
                new SignUp() { Username = "test"},
                new SignUp() { Password = "password1"}
            };

            var sut = CreateTestSubject();
            var result = await sut.CreateUser(index == null ? null : signups[(int)index]);
            
            Assert.IsType<BadRequestResult>(result);

            this.AutoMocker.GetMock<IUserCreator>()
                .Verify(x => x.CreateUser(It.IsAny<SignUp>()), Times.Never);

            this.AutoMocker.GetMock<ICookieAdder>()
                .Verify(x => x.AddCookie(It.IsAny<HttpContext>(), "bearer", It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task CreateUserReturnsOkWithErrorIfUserIsAlreadyFound()
        {
            var signUp = new SignUp() { Username = "Bob", Password = "Bob123" };
            this.AutoMocker.GetMock<IGetLoginByUsernameQuery>()
                .Setup(x => x.Execute(signUp.Username))
                .ReturnsAsync(new DataAccess.Entities.Login());

            var sut = CreateTestSubject();

            var result = await sut.CreateUser(signUp) as OkObjectResult;

            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task CreateUserReturnsOkIfNoErrorIsThrown()
        {
            var stubSignUp = new SignUp() { Username = "test", Password = "Bobby1" };
            var stubLogin = new DataAccess.Entities.Login() { Key = 1 };

            this.AutoMocker.GetMock<IGetLoginByUsernameQuery>()
                .Setup(x => x.Execute(stubSignUp.Username))
                .ReturnsAsync((DataAccess.Entities.Login)null);
            this.AutoMocker.GetMock<IUserCreator>()
                .Setup(x => x.CreateUser(stubSignUp))
                .ReturnsAsync(stubLogin);

            this.AutoMocker.GetMock<ITokenBuilder>()
                .Setup(x => x.BuildToken(stubLogin.Key))
                .Returns("1111");

            var sut = CreateTestSubject();
            var result = await sut.CreateUser(stubSignUp) as StatusCodeResult;

            this.AutoMocker.GetMock<IUserCreator>()
                 .Verify(x => x.CreateUser(stubSignUp), Times.Once);
            this.AutoMocker.GetMock<ICookieAdder>()
                .Verify(x => x.AddCookie(It.IsAny<HttpContext>(), "bearer", "1111"), Times.Once);

            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task CreateUserReturns500ErrorIfErrorIsThrown()
        {
            var stubSignUp = new SignUp() { Username = "test", Password = "Bobby1"};

            this.AutoMocker.GetMock<IUserCreator>()
                 .Setup(x => x.CreateUser(stubSignUp))
                 .Throws(new Exception());
            this.AutoMocker.GetMock<IGetLoginByUsernameQuery>()
                .Setup(x => x.Execute(stubSignUp.Username))
                .ReturnsAsync((DataAccess.Entities.Login)null);
                
            var sut = CreateTestSubject();
            var result = await sut.CreateUser(stubSignUp);

            this.AutoMocker.GetMock<IUserCreator>()
                 .Verify(x => x.CreateUser(stubSignUp), Times.Once);

            var statusCodeResult = result as StatusCodeResult;

            Assert.Equal(500, statusCodeResult.StatusCode);
            this.AutoMocker.GetMock<ICookieAdder>()
                .Verify(x => x.AddCookie(It.IsAny<HttpContext>(), "bearer", It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task LoginReturnsUnauthorisedIfUsernamefNotFound()
        {
            var stubLogin = new Models.Login() { Username = "Pete", Password = "Peter123" };

            this.AutoMocker.GetMock<IGetLoginByUsernameQuery>()
                .Setup(x => x.Execute(stubLogin.Username))
                .ReturnsAsync((DataAccess.Entities.Login)null);

            var sut = CreateTestSubject();
            var result = await sut.Login(stubLogin);

            Assert.IsType<UnauthorizedResult>(result);
            this.AutoMocker.GetMock<ICookieAdder>()
                .Verify(x => x.AddCookie(It.IsAny<HttpContext>(), "bearer", It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task LoginReturnsOkAuthorisedIfPasswordIsCorrect()
        {
            var stubLogin = new Models.Login() { Username = "Pete", Password = "xfdfdf" };
            var stubUser = new DataAccess.Entities.Login() { Key = 1, Username = "Pete", Password = "$2a$12$ipE8DZ56KkKh9sH1V2eGZO2XlZFQkA3LlbI1lTn2ABGAEM12MbAH6"};


            this.AutoMocker.GetMock<IGetLoginByUsernameQuery>()
                .Setup(x => x.Execute(stubLogin.Username))
                .ReturnsAsync(stubUser);
            this.AutoMocker.GetMock<ITokenBuilder>()
                .Setup(x => x.BuildToken(stubUser.Key))
                .Returns("1111");

            var sut = CreateTestSubject();
            sut.ControllerContext = new ControllerContext() { HttpContext = new DefaultHttpContext() };
            var result = await sut.Login(stubLogin);

            Assert.IsType<OkResult>(result);
            this.AutoMocker.GetMock<ICookieAdder>()
                .Verify(x => x.AddCookie(It.IsAny<HttpContext>(), "bearer", "1111"), Times.Once);
        }

        [Fact]
        public async Task LoginReturnsUnauthorisedIfPasswordIsIncorrect()
        {
            var stubLogin = new Models.Login() { Username = "Pete", Password = "WrongPassword" };
            var stubUser = new DataAccess.Entities.Login() { Username = "Pete", Password = "$2a$12$ipE8DZ56KkKh9sH1V2eGZO2XlZFQkA3LlbI1lTn2ABGAEM12MbAH6"};

            this.AutoMocker.GetMock<IGetLoginByUsernameQuery>()
                .Setup(x => x.Execute(stubLogin.Username))
                .ReturnsAsync(stubUser);

            var sut = CreateTestSubject();
            var result = await sut.Login(stubLogin);

            Assert.IsType<UnauthorizedResult>(result);
            this.AutoMocker.GetMock<ICookieAdder>()
                .Verify(x => x.AddCookie(It.IsAny<HttpContext>(), "bearer", It.IsAny<string>()), Times.Never);
        }
    }
}
