namespace Banterdome.Api.Authentication.Tests
{
    using Moq.AutoMock;

    public abstract class TestBase<T> where T : class
    {
        protected readonly AutoMocker AutoMocker = new AutoMocker();

        protected T CreateTestSubject()
        {
            return this.AutoMocker.CreateInstance<T>();
        }
    }
}
