﻿using Moq;
using Banterdome.Api.Authentication.Business;
using Banterdome.Api.Authentication.DataAccess.Commands;
using Banterdome.Api.Authentication.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Banterdome.Api.Authentication.Tests.Business
{
    public class UserCreatorTests : TestBase<UserCreator>
    {
        [Fact]
        public async Task CreateUserHashesPasswordAndInsertsIntoDatabase()
        {
            var stubSignup = new SignUp { Username = "John@msn.com", Password = "SecurePassword" };


            var sut = CreateTestSubject();

            await sut.CreateUser(stubSignup);

            this.AutoMocker.GetMock<IInsertEntityCommand>()
                .Verify(me => me.Execute(It.Is<DataAccess.Entities.Login>(
                    x => x.Username == stubSignup.Username
                    )), Times.Once);
        }
    }
}
