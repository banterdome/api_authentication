﻿namespace Banterdome.Api.Authentication.Models
{
    public class SignUp
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
