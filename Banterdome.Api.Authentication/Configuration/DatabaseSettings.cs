﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Banterdome.Api.Authentication.Configuration
{
    public class DatabaseSettings
    {
        public string AuthConnectionString { get; set; }
    }
}
