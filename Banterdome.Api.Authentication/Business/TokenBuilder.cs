﻿using Banterdome.Api.Authentication.Business;
using Banterdome.Api.Authentication.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Banterdome.Api.Authentication.Business
{
    public class TokenBuilder : ITokenBuilder
    {
        private readonly TokenSettings tokenSettings;

        public TokenBuilder(TokenSettings tokenSettings)
        {
            this.tokenSettings = tokenSettings;
        }

        public string BuildToken(int id)
        {
            var claims = new[]
            {
                new Claim("id", id.ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSettings.Key));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(tokenSettings.Issuer,
                                             tokenSettings.Audience,
                                             claims,
                                             signingCredentials: credentials,
                                             expires: DateTime.Now.AddDays(1));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
