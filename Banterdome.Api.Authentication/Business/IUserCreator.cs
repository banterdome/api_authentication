﻿using System.Threading.Tasks;
using Banterdome.Api.Authentication.Models;
using Login = Banterdome.Api.Authentication.DataAccess.Entities.Login;

namespace Banterdome.Api.Authentication.Business
{
    public interface IUserCreator
    {
        Task<Login> CreateUser(SignUp signup);
    }
}
