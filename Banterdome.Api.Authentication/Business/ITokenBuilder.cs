﻿namespace Banterdome.Api.Authentication.Business
{
    public interface ITokenBuilder
    {
        string BuildToken(int id);
    }
}
