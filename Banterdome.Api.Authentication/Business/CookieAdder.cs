﻿namespace Banterdome.Api.Authentication.Business
{
    using Microsoft.AspNetCore.Http;
    using System;

    public class CookieAdder : ICookieAdder
    {
        public void AddCookie(HttpContext httpContext, string key, string value)
        {
            var cookieOptions = new CookieOptions()
            {
                Secure = true,
                HttpOnly = true,
                SameSite = SameSiteMode.None,
                Domain = ".banterdo.me",
                Expires = DateTime.Now.AddDays(1)
            };

            httpContext.Response.Cookies.Append(key, value, cookieOptions);
        }
    }
}
