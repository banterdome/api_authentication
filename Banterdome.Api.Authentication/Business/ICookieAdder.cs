﻿namespace Banterdome.Api.Authentication.Business
{
    using Microsoft.AspNetCore.Http;

    public interface ICookieAdder
    {
        public void AddCookie(HttpContext httpContext, string key, string value);
    }
}
