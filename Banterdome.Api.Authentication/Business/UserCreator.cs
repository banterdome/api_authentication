﻿using System.Threading.Tasks;
using Banterdome.Api.Authentication.Models;
using System.Data;
using Banterdome.Api.Authentication.DataAccess.Commands;
using BCryptNet = BCrypt.Net.BCrypt;
using Login = Banterdome.Api.Authentication.DataAccess.Entities.Login;

namespace Banterdome.Api.Authentication.Business
{
    public class UserCreator : IUserCreator
    {
        private readonly IInsertEntityCommand insertEntityCommand;

        public UserCreator(IInsertEntityCommand insertEntityCommand)
        {
            this.insertEntityCommand = insertEntityCommand;
        }

        public async Task<Login> CreateUser(SignUp signup)
        {
            var login = new DataAccess.Entities.Login
            {
                Password = BCrypt.Net.BCrypt.HashPassword(signup.Password, workFactor: 13),
                Username = signup.Username,
            };

            return await insertEntityCommand.Execute(login);
        }
    }
}
