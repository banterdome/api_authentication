﻿namespace Banterdome.Api.Authentication.DataAccess
{
    using Microsoft.EntityFrameworkCore;
    using Banterdome.Api.Authentication.DataAccess.Entities;
    using System;
    using System.Threading.Tasks;

    public interface IDataContext : IDisposable
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        DbSet<Login> Logins { get; set; }

        Task<int> SaveChangesAsync();
    }
}
