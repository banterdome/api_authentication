﻿namespace Banterdome.Api.Authentication.Data
{
    using Microsoft.EntityFrameworkCore;
    using Banterdome.Api.Authentication.DataAccess.Entities;
    using Banterdome.Api.Authentication.DataAccess;
    using System.Threading.Tasks;

    public class DataContext : DbContext, IDataContext
    {
        
         public DataContext(
                DbContextOptions options)
                : base(options)
         {
         }

        public DbSet<Login> Logins { get; set; }

        public Task<int> SaveChangesAsync()
        {
           return base.SaveChangesAsync();   
        }
    }
}
