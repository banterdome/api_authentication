﻿namespace Banterdome.Api.Authentication.DataAccess
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using Banterdome.Api.Authentication.Configuration;
    using Banterdome.Api.Authentication.Data;

    [ExcludeFromCodeCoverage]
    public class DataContextFactory : IDataContextFactory
    {
        private readonly DatabaseSettings databaseSettings;

        public DataContextFactory(DatabaseSettings databaseSettings)
        {
            this.databaseSettings = databaseSettings;
        }
    
        public IDataContext Create()
        {
            var contextOptionsBuilder = new DbContextOptionsBuilder();
            contextOptionsBuilder.UseSqlServer(databaseSettings.AuthConnectionString);
            return new DataContext(contextOptionsBuilder.Options);
        }
    }
}
