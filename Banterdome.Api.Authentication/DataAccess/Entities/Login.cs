﻿namespace Banterdome.Api.Authentication.DataAccess.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    [Table("LOG_LOGINS", Schema = "dbo")]
    public class Login
    {
        [Key,Column("LOG_KEY")]
        public int Key { get; set; }

        [Column("LOG_PASSWORD")]
        public string Password { get; set; }

        [Column("LOG_USERNAME")]
        public string Username { get; set; }
    }
}
