﻿using Microsoft.EntityFrameworkCore;
using Banterdome.Api.Authentication.DataAccess.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Banterdome.Api.Authentication.DataAccess.Query
{
    public class GetLoginByUsernameQuery : IGetLoginByUsernameQuery
    {
        private readonly IDataContextFactory dataContextFactory;

        public GetLoginByUsernameQuery (IDataContextFactory dataContextFactory)
        {
            this.dataContextFactory = dataContextFactory;
        }

        public async Task<Login> Execute(string username)
        {
            using (var context = this.dataContextFactory.Create())
            {
                return await context.Logins.Where(x => x.Username.ToLower() == username.ToLower()).FirstOrDefaultAsync();
            }
        }
    }
}
