﻿using Banterdome.Api.Authentication.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Banterdome.Api.Authentication.DataAccess.Query
{
    public interface IGetLoginByUsernameQuery
    {
        Task<Login> Execute(string username);
    }
}
