﻿using System.Threading.Tasks;

namespace Banterdome.Api.Authentication.DataAccess.Commands
{
    public class InsertEntityCommand : IInsertEntityCommand
    {
        private readonly IDataContextFactory dataContextFactory;

        public InsertEntityCommand(IDataContextFactory dataContextFactory)
        {
            this.dataContextFactory = dataContextFactory;
        }

        public async Task<T> Execute<T>(T entity) where T : class
        {
            using(var context = this.dataContextFactory.Create())
            {
                context.Set<T>().Add(entity);
                await context.SaveChangesAsync();

                return entity;
            }
        }
    }
}
