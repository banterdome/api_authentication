﻿using System.Threading.Tasks;

namespace Banterdome.Api.Authentication.DataAccess.Commands
{
    public interface IInsertEntityCommand
    {
        Task<T> Execute<T>(T entity) where T : class;
    }
}
