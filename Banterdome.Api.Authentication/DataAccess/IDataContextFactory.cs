﻿namespace Banterdome.Api.Authentication.DataAccess
{
    public interface IDataContextFactory
    {
        IDataContext Create();
    }
}
