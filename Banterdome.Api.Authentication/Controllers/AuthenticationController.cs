﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Banterdome.Api.Authentication.Models;
using Banterdome.Api.Authentication.Business;
using Banterdome.Api.Authentication.DataAccess.Query;
using BCryptNet = BCrypt.Net.BCrypt;

namespace Banterdome.Api.Authentication.Controllers
{
    
    [Route("[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IUserCreator userCreator;
        private readonly IGetLoginByUsernameQuery getLoginByUsernameQuery;
        private readonly ITokenBuilder tokenBuilder;
        private readonly ICookieAdder cookieAdder;
        private const string cookieKey = "bearer";

        public AuthenticationController(IUserCreator userCreator, IGetLoginByUsernameQuery getLoginByUsernameQuery, ITokenBuilder tokenBuilder, ICookieAdder cookieAdder)
        {
            this.userCreator = userCreator;
            this.getLoginByUsernameQuery = getLoginByUsernameQuery;
            this.tokenBuilder = tokenBuilder;
            this.cookieAdder = cookieAdder;
        }

        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUser([FromBody]SignUp signup)
        {
            if(signup == null || String.IsNullOrEmpty(signup.Username)|| String.IsNullOrEmpty(signup.Password))
            {
                return BadRequest();
            }

            try
            {
                //check if username exists
                if(await getLoginByUsernameQuery.Execute(signup.Username) != null)
                {
                    return new OkObjectResult(new { Error = "Username already exists" });
                }

                var user = await userCreator.CreateUser(signup);
                this.cookieAdder.AddCookie(HttpContext, cookieKey, this.tokenBuilder.BuildToken(user.Key));
                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody]Login login)
        {

            if (login == null || String.IsNullOrEmpty(login.Username) || String.IsNullOrEmpty(login.Password))
            {
                return BadRequest();
            }

            try
            {
                //check username exists
                var user = await getLoginByUsernameQuery.Execute(login.Username);

                if (user == null)
                {
                    return Unauthorized();
                }

                //check against stored password
                if (BCryptNet.Verify(login.Password, user.Password))
                {
                    this.cookieAdder.AddCookie(HttpContext, cookieKey, this.tokenBuilder.BuildToken(user.Key));
                    return Ok();
                }

                return Unauthorized();
            } 
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
