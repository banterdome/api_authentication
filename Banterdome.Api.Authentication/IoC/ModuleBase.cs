﻿namespace Banterdome.Api.Authentication.IoC
{
    using Autofac;
    using Microsoft.Extensions.Configuration;

    public class ModuleBase : Module
    {
        protected readonly IConfiguration Configuration;

        public ModuleBase(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(typeof(Startup).Assembly)
                   .AsImplementedInterfaces();
        }
    }

}
