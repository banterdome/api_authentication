﻿namespace Banterdome.Api.Authentication.IoC
{
    using Autofac;
    using Banterdome.Api.Authentication.Configuration;
    using Microsoft.Extensions.Configuration;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class AuthenticationModule : ModuleBase
    {
        public AuthenticationModule(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(k => this.GetTokenSettings())
                .SingleInstance();

            builder.Register(k => this.GetDatabaseSettings())
                   .SingleInstance();
        }

        private DatabaseSettings GetDatabaseSettings()
        {
            // builds temporary environment variable configuration
            var ec = new ConfigurationBuilder()
                     .AddEnvironmentVariables()
                     .Build();

            // maps values to the selected type
            var result = ec.Get<DatabaseSettings>();

            return result;
        }

        private TokenSettings GetTokenSettings()
        {
            // builds temporary environment variable configuration
            var ec = new ConfigurationBuilder()
                     .AddEnvironmentVariables()
                     .Build();

            // maps values to the selected type
            var result = ec.Get<TokenSettings>();

            return result;
        }

    }
}
